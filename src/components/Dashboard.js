import React, { useEffect, useState } from 'react'
import { Container, Row, Col } from 'react-bootstrap';
import { connect, useDispatch } from 'react-redux';
import { postRequestAsync } from '../api/postRequest';
import '../Inspection/inspectionList.scss'
import history from '../js/history';
import { set_brand_id, set_brand_name, set_product_id, set_product_name } from '../store/reducer';

function HomeScreen(props) {
    return (
        <Container>
            <Row>
                <Col>
                    <span className='summary-card-title-ques'>
                        {header}
                    </span>
                </Col>
            </Row>
       </Container>
    );
}
export default HomeScreen;