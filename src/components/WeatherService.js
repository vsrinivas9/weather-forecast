// WeatherService.js
import axios from 'axios';

const API_KEY = '1635890035cbba097fd5c26c8ea672a1';
const GEOCODING_API_BASE_URL = 'https://api.openweathermap.org/geo/1.0/direct';
const WEATHER_API_BASE_URL = 'https://api.openweathermap.org/data/2.5/forecast';

const getCoordinatesByCity = async (city) => {
  try {
    const url = `${GEOCODING_API_BASE_URL}?q=${city}&limit=1&appid=${API_KEY}`;
    const response = await axios.get(url);
    const [location] = response.data;
    return {
      lat: location.lat,
      lon: location.lon
    };
  } catch (error) {
    throw error;
  }
};

const getWeatherByCity = async (city) => {
  try {
    const coordinates = await getCoordinatesByCity(city);
  //  console.log("coordinates", coordinates)
    const url =
      coordinates && coordinates.lat && coordinates.lon
        ? `${WEATHER_API_BASE_URL}?lat=${coordinates.lat}&lon=${coordinates.lon}&appid=${API_KEY}`
        : `${WEATHER_API_BASE_URL}?q=${city}&appid=${API_KEY}`;
    const response = await axios.get(url);
    return response.data;
  } catch (error) {
    console.error("Error fetching weather data:", error);
    throw error;
  }
};
const WeatherService = {
  getCoordinatesByCity,
  getWeatherByCity
};

export default WeatherService;
