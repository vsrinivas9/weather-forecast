// App.js
import React, { useEffect, useState } from 'react';
import WeatherService from './components/WeatherService';
import { Container, Row, Col } from 'react-bootstrap';
import './home.css'
import Spinner from 'react-bootstrap/Spinner';
function App() {
  const [city, setCity] = useState('');
  const [weatherData, setWeatherData] = useState(null);
  const [isMobile, setIsMobile] = useState(window.innerWidth <= 750);
  const [showLoader, setShowLoader] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= 750);
    };

    // Add event listener for window resize
    window.addEventListener('resize', handleResize);

    // Clean up the event listener on component unmount
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const formattedDate = (timestamp) => {
    const date = new Date(timestamp * 1000);
    return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
  }

  const prepareDataObj = (data) => {
    if (!data || !data.list || !Array.isArray(data.list)) {
      console.error("Error: Invalid data format");
      return null;
    }

    return data.list.map((item) => {
      const { dt: date, main } = item;
      if (!date || !main) {
        console.warn("Warning: Missing properties in data object");
        return null;
      }
      return {
        date: formattedDate(date),
        pressure: main.pressure,
        humidity: main.humidity,
        min: main.temp_min,
        max: main.temp_max,
      };
    }).filter(item => item !== null);
  }
  // Util functions
function removeDuplicatesByDate(data) {
  return data.reduce((accumulator, currentEntry) => {
    const existingEntry = accumulator.find((entry) => entry.date === currentEntry.date);
    if (!existingEntry) {
      accumulator.push(currentEntry);
    }
    return accumulator;
  }, []);
}

function sortDataByDate(data) {
  return data.sort((a, b) => new Date(a.date) - new Date(b.date));
}

function getNextFiveDaysData(data) {
  return data.slice(0, 5);
}

  const getWeather = async () => {
    try {
      setShowLoader(true);
      const data = await WeatherService.getWeatherByCity(city);
      const dataObj = prepareDataObj(data);
      const filteredData = removeDuplicatesByDate(dataObj);
      const sortedData = sortDataByDate(filteredData);
      const nextFiveDaysData = getNextFiveDaysData(sortedData);
      setWeatherData(nextFiveDaysData);
      console.log("dataObj", nextFiveDaysData);
      setShowLoader(false)
    } catch (error) {
      setShowLoader(false)
      console.error('Error fetching weather data:', error);

    }
  };

  return (
    <div className="App">
      <Container fluid>
        <Row style={{ padding: '20px 30px', marginTop: '10px' }}>
          <Col xs={12} lg={4} className='wfFlex wfVcenter wfHcenter'>
            <div className='wfheader'>Weather in your city</div>
          </Col>
          <Col xs={12} lg={6} className='wfFlex wfHcenter wfVcenter'>
            <input className='wfSearch' placeholder='Enter City' type="text" value={city} onChange={(e) => setCity(e.target.value)} />
            {!isMobile ? (
              <>
                <div style={{
                  marginLeft: '20px'
                }} className="wfsearchBtn" onClick={getWeather}>
                  Search
                </div>
                {
                  showLoader ? <div style={{ padding: '10px', width: '50px', height: '50px' }}>
                    <Spinner animation="border" role="status">
                    </Spinner>
                  </div> : ""
                }


              </>

            ) : ''}


          </Col>
          {
            isMobile ? (
              <Col xs={12} className="wfFlex wfHcenter wfVcenter">
                <div className="wfsearchBtn" onClick={getWeather}>
                  Search
                </div>
                {
                  showLoader ? <div style={{ padding: '10px', width: '50px', height: '50px', marginLeft: '10px', marginTop: '25px' }}>
                    <Spinner animation="border" role="status">
                    </Spinner>
                  </div> : ""
                }
              </Col>
            ) : ""
          }

        </Row>
        <Row>
        </Row>
        <Row >
          <Col className='nopad'>
            <Container fluid className='ins-grid'>
              {
                weatherData && (weatherData.map((data, index) => {
                  return <div className='ins-grid-items' key={index}>

                    <Row>
                      <Col id='dateBlock'>
                        {`Date: ${data.date} `}
                      </Col>
                    </Row>
                    <Row>
                      <Col className='tempBlock' >Temperature</Col>
                    </Row>
                    <Row>
                      <Col className='wfMinMaxBlock rtBorder' xs={6}>Min</Col>
                      <Col className='wfMinMaxBlock' cs={6}>Max</Col>
                    </Row>
                    <Row>
                      <Col className='wfMinMaxBlock rtBorder' xs={6}>{`${data.min}K`}</Col>
                      <Col className='wfMinMaxBlock' cs={6}>{`${data.max}K`}</Col>
                    </Row>
                    <Row>
                      <Col className='wfpresureHumidityBlock rtBorder' xs={6}>Pressure</Col>
                      <Col className='wfpresureHumidityBlock' cs={6}>{data.pressure}</Col>
                    </Row>
                    <Row>
                      <Col className='wfpresureHumidityBlock rtBorder' xs={6}>Humidity</Col>
                      <Col className='wfpresureHumidityBlock' cs={6}>{data.humidity}</Col>
                    </Row>
                  </div>
                })

                )}

            </Container>

          </Col>
        </Row>


      </Container>
    </div>
  );
}

export default App;
